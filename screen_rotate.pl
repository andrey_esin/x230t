#!/usr/bin/perl -w

my $current = `xrandr --current --screen 0`;
my $dev_trackpoint_id = `xinput list --id-only "TPPS/2 IBM TrackPoint"`; chop($dev_trackpoint_id);
my $dev_touchpad_id = `xinput list --id-only "SynPS/2 Synaptics TouchPad"`; chop($dev_touchpad_id);
my $dev_stylus_id = `xinput list --id-only "Wacom ISDv4 90 Pen stylus"`; chop($dev_stylus_id);
my $dev_eraser_id = `xinput list --id-only "Wacom ISDv4 90 Pen eraser"`; chop($dev_eraser_id);

sub startsWith {
	return index($_[0], $_[1]) == 0 ? 1 : 0;
}

sub closeModeDialog {
	system ("kill -9 \$YAD_PID");
}

sub tablet_mode {
	# Close mode dialog
	closeModeDialog;

	# Rotate screen
	system("xrandr -o inverted");

	# Disable Trackpoint
	system("xinput disable $dev_trackpoint_id");

	# Disable Touchpad
	system("xinput disable $dev_touchpad_id");

	# Rotate Stylus
	# 270 - Stylus property. 0 - 0°, 1 - 90°, 2 - 270°, 3 - 180°
	system("xinput set-prop $dev_stylus_id 270 3");
	# Rotate Eraser
	system("xinput set-prop $dev_eraser_id 270 3");
}

sub reader_mode {
	# Close mode dialog
	closeModeDialog;

	# Rotate screen
	system("xrandr -o right");

	# Disable Trackpoint
	system("xinput disable $dev_trackpoint_id");

	# Disable Touchpad
	system("xinput disable $dev_touchpad_id");

	# Rotate Stylus
	# 270 - Stylus property. 0 - 0°, 1 - 90°, 2 - 270°, 3 - 180°
	system("xinput set-prop $dev_stylus_id 270 1");
	# Rotate Eraser
	system("xinput set-prop $dev_eraser_id 270 1");
}


sub normal_mode {
	# Rotate screen
	system("xrandr -o normal");

	# Enable Trackpoint
	system("xinput enable $dev_trackpoint_id");

	# Enable Touchpad
	system("xinput enable $dev_touchpad_id");

	# Rotate Stylus
	# 270 - Stylus property. 0 - 0°, 1 - 90°, 2 - 270°, 3 - 180°
	system("xinput set-prop $dev_stylus_id 270 0");
	# Rotate Eraser
	system("xinput set-prop $dev_eraser_id 270 0");
}

sub check_yad {
	system("yad --version > /dev/null 2>&1");
	my $res = $? >> 8;
	if ($res == 127){
		print "Can't find YAD (Yet Another Dialog)\nBut you can install it from https://sourceforge.net/projects/yad-dialog/\n";
		exit 1;
	}
}

sub showModeDialog {
	system("yad --timeout=5 --skip-taskbar --icons --item-width=96 --title=↺ --read-dir=/home/lastik/src/x230t/images --single-click --no-buttons --fixed --center --undecorated --on-top --borders=0 --height=197 --width=345");
	exit 0;
}

sub amIAlone { # Checks for only 1 copy of this script
	my $ps = `ps auxwww | grep $0 | grep -v grep | wc -l`;

	if ($ps > 1) {
		exit;
	}
}

if ($#ARGV == -1) {
	# We are not in normal mode
	if ($current =~ /(.*?)LVDS1 connected ([x+0-9]+) (.*?)? \((.*?)/sig){ 
		my $mode = $3;
		if ($mode eq "inverted" || $mode eq "left" || $mode eq "right" ) {
			normal_mode;
			exit 0;
		}
	}

	amIAlone;
	check_yad;
	showModeDialog;
	exit 0;
}  


my $params = $ARGV[0];
if (startsWith($params, "--mode")){
	my $choosen_mode = substr($params, index($params, "=") + 1);
	if ($choosen_mode eq "tablet"){
		tablet_mode;
	}
	if ($choosen_mode eq "reader"){
		reader_mode;
	}


#	print "$choosen_mode\n";
}

exit 0;
